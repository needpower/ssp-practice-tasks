
public class Main {
	static java.util.Random RNG = new java.util.Random();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		int[] arr = new int[10];
		int range=100;
		
		for(int i = 0; i < arr.length; i++) {
			arr[i] = RNG.nextInt(range);
		}
		
		System.out.print("initial random array: ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+" ");
		}
		bubbleSort(arr);
		reverseArray(arr);
		System.out.println();
		
		System.out.print("sorted array in reverse order: ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
	
	
	public static void bubbleSort(int[] array){
	    /*Внешний цикл каждый раз сокращает фрагмент массива, 
	      так как внутренний цикл каждый раз ставит в конец
	      фрагмента максимальный элемент*/   
	    for(int i = array.length-1 ; i > 0 ; i--){
	        for(int j = 0 ; j < i ; j++){
	            /*Сравниваем элементы попарно, 
	              если они имеют неправильный порядок, 
	              то меняем местами*/
	            if( array[j] > array[j+1] ){
	                int tmp = array[j];
	                array[j] = array[j+1];
	                array[j+1] = tmp;
	            }
	        }
	    }
	}
	
	/*Выводим последовательность чисел в массиве в порядке убывания*/
	public static void reverseArray(int[] a){
		for (int i = 0; i < a.length/2; i++) {
			a[i] = a[i] + a[a.length-1-i];
			a[a.length-1-i] = a[i] - a[a.length-1-i];
			a[i] = a[i] - a[a.length-1-i];
		}
	}
	
}
